﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelChanger : MonoBehaviour {
    public Animator animator;
    private int toLoad;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (GameController.instance.isFinished == true)
        {
            FadeToLevel(0);
        }
        if (GameController.instance.success == true)
        {
            FadeToLevel(2);
           //GameController.instance.success = false;
        }

        if (GameController.instance.successTwo == true)
        {
            FadeToLevel(3);
            //GameController.instance.successTwo = false;
        }
		
	}
    public void FadeToLevel(int index)
    {
        toLoad = index;
        animator.SetTrigger("fadeOut");
        OnFadeComplete();
    }
    public void OnFadeComplete()
    {
        SceneManager.LoadScene(toLoad);
    }
}
