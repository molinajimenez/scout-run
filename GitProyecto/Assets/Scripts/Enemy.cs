﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

    // Use this for initialization
    public GameObject targetChar;
    SpriteRenderer thisChar;
    Vector2 totalDistance;
    public float toleratedDistance;
    Animator anim;
    Vector2 bulletPos;
    AudioSource audioSource;
    public float remainingTime=5;
    public float timeRemainingReset=5;
    // simulado de bala.
    public GameObject bullet;
    public GameObject bulletLeft;
    void Start () {
        thisChar = GetComponent<SpriteRenderer>();
        toleratedDistance = 8.5f;
        anim = GetComponent<Animator>();
        audioSource = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {

        // calculo de distancia entre enemigo y personaje.
        totalDistance = new Vector2(targetChar.transform.position.x - thisChar.transform.position.x, targetChar.transform.position.y - thisChar.transform.position.y);
        remainingTime -= Time.deltaTime;
        // si el enemigo esta a cierta distancia, comienza a atacar.
        if (totalDistance.x > thisChar.transform.position.x)
        {
            thisChar.flipX = false ;
        }
        else
        {
            thisChar.flipX = true;
        }

        if (totalDistance.x <= toleratedDistance)
        {

            if (remainingTime <= 0)
            {
                remainingTime = timeRemainingReset;
                anim.SetBool("engage", true);
                Attack();
            }
            

        } else
        {
            anim.SetBool("engage", false);
        }
        
        
		
	}
    void Attack()
    {
        audioSource.Play();
        bulletPos = new Vector2(0, 0);
        if (thisChar.flipX == true)
        {
            bulletPos += new Vector2(thisChar.transform.position.x, thisChar.transform.position.y);
            Instantiate(bulletLeft, bulletPos, Quaternion.identity);

        }
        else
        {
            bulletPos += new Vector2(thisChar.transform.position.x, thisChar.transform.position.y);
            Instantiate(bullet, bulletPos, Quaternion.identity);
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("bullet"))
        {
            Destroy(gameObject);
        }
    }
}
