﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class EnemySpawn : MonoBehaviour
{
    public GameObject enemy;
    public float spawnTime = 4f;
    public float elapsedTime = 0f;
    public GameObject cam;

    int enemyLimit =0;
    public int waitTimeMs;

    // Use this for initialization
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (GameController.instance.isFinished == false)
        {
            if (elapsedTime < spawnTime)
            {
                elapsedTime += Time.deltaTime;
            }
            else
            {
                if (enemyLimit < 5)
                {
                    Instantiate(enemy, new Vector3(cam.transform.position.x + 10, cam.transform.position.y + 1, 0), Quaternion.identity);
                    elapsedTime = 0;
                    enemyLimit += 1;
                }

            }

        }
        else
        {
            //a este punto ya se debio de cambiar de escena :D
        }

    }
}
