﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIFade : MonoBehaviour {

    public CanvasGroup ui;

    public void FadeIn(CanvasGroup canvas){
        StartCoroutine(Fade(ui, ui.alpha, 1));
        canvas.interactable = true;
    }

    public void FadeOut(CanvasGroup canvas){
        StartCoroutine(Fade(ui, ui.alpha, 0));
        canvas.interactable = false;

    }

    public IEnumerator Fade(CanvasGroup canvas, float start, float end, float lerpTime = 0.5f){
        float _timeStartedLerping = Time.time;
        float timeSinceStarted = Time.time - _timeStartedLerping;
        float percentageComplete = timeSinceStarted / lerpTime;



        while (true)
        {
            timeSinceStarted = Time.time - _timeStartedLerping;
            percentageComplete = timeSinceStarted / lerpTime;

            float currentV = Mathf.Lerp(start, end, percentageComplete);
            canvas.alpha = currentV;

            if (percentageComplete >= 1){
                break;
            }

            yield return new WaitForEndOfFrame();
            print("si funciono");
        }

    } 

}
