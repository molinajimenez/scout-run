﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tank : MonoBehaviour {
    AudioSource audioSource;
    SpriteRenderer thisChar;
    Vector2 bulletPos;

    public GameObject target;
    public float remainingTime = 5;
    public float timeRemainingReset = 5;
    public GameObject bulletLeft;

    public int health = 220;
    // Use this for initialization
    void Start () {
        audioSource = GetComponent<AudioSource>();
        thisChar = GetComponent<SpriteRenderer>();
		
	}
	
	// Update is called once per frame
	void Update () {
        remainingTime -= Time.deltaTime;
        if (remainingTime <= 0)
        {
            remainingTime = timeRemainingReset;
            Attack();
        }

    }
    void Attack()
    {
        audioSource.Play();
        bulletPos = new Vector2(-0.9f, 1.2f);
        if (thisChar.flipX == true)
        {
            bulletPos += new Vector2(thisChar.transform.position.x, target.transform.position.y);
            Instantiate(bulletLeft, bulletPos, Quaternion.identity);

        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("bullet"))
        {
            health -= 1;

            if (health <= 0)
            {
                Destroy(gameObject);
                GameController.instance.isFinished = true;
            }
        }
    }
}
