﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class HealthBar : MonoBehaviour {
    float hp, maxHp = 100f;
    public Image healthBar;
    public static HealthBar instance;

	// Use this for initialization
	void Start () {
        instance = this;
        
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void damage(float amount)
    {
        GameController.instance.charHealth -= amount;
        Debug.Log(GameController.instance.charHealth);
        healthBar.fillAmount =GameController.instance.charHealth / maxHp;
    }
    public void heal(float amount)
    {
        GameController.instance.charHealth += amount;
        Debug.Log(GameController.instance.charHealth);
        healthBar.fillAmount = GameController.instance.charHealth / maxHp;
    }
}
