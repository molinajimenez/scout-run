﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBullet : MonoBehaviour
{
    public float velx = 40f;
    float velY = 0f;
    Rigidbody2D rb;

    // Use this for initialization
    void Start()
    {

        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        rb.velocity = new Vector2(velx, velY);
        Destroy(gameObject, 1.5f);

    }
    //Destruye el objeto al impactar;
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("char"))
        {
            Destroy(gameObject, 0);
        }
    }
}
