﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Character : MonoBehaviour {
    //components
    Rigidbody2D rb2d;
    SpriteRenderer sr;
    public Camera cam;
    //movement
    private float speed = 5f;
    public float jumpForce = 1f;
    public float secondJump = 0.3f;
    //multimedia
    Animator anim;
    AudioSource src;

    //requerido para salto
    public GameObject myFeetBoii;
    public LayerMask layerMask;
    bool extraJump = false;
    float count = 0;
    // estados
    bool right = false;
    bool left = false;
    bool up = false;

    //health 1.0
    public float charHealth = 100f;
    public float bulletdmg;
    public float cure = 25f;
    // Use this for initialization
    void Start () {
        src = GetComponent<AudioSource>();
        anim = GetComponent<Animator>();
        rb2d = GetComponent<Rigidbody2D>();
        sr = GetComponent<SpriteRenderer>();
        cam.transform.position = new Vector3(rb2d.transform.position.x, cam.transform.position.y, cam.transform.position.z);    
    }
	
	// Update is called once per frame
	void Update () {
        if (right)
        {
            rb2d.transform.Translate(Vector3.right * Time.deltaTime * speed);
            sr.flipX = false;
            cam.transform.position = new Vector3(rb2d.transform.position.x, cam.transform.position.y, cam.transform.position.z);
            anim.SetFloat("Speed", Mathf.Abs(1.0f));
        }
       
        if (left)
        {
            rb2d.transform.Translate(Vector3.left * Time.deltaTime * speed);
            sr.flipX = true;
            cam.transform.position = new Vector3(rb2d.transform.position.x, cam.transform.position.y, cam.transform.position.z);
            anim.SetFloat("Speed", Mathf.Abs(1.0f));
        }
        
        if (up)
        {
            RaycastHit2D raycast = Physics2D.Raycast(myFeetBoii.transform.position, Vector2.down, 1f, layerMask);
            if (raycast.collider != null)
            {
                count += 1;
                rb2d.AddForce(Vector2.up * jumpForce);
                extraJump = true;
                Debug.Log(count);
            } else
            {
                if (extraJump)
                {
                    rb2d.AddForce(Vector2.up * secondJump);
                    extraJump = false;
                }
                else
                {
                    //nada
                }


            }
        }

        //Mata al personaje si cae mucho.
        if (rb2d.transform.position.y <= -20)
        {
            GameController.instance.isFinished = true;
        }

    }


private void OnCollisionEnter2D(Collision2D collision)
{
    if (collision.gameObject.CompareTag("enemyBullet"))
    {
        HealthBar.instance.damage(bulletdmg);

        if (GameController.instance.charHealth <= 0)
        {
            GameController.instance.isFinished = true;
        }

    }

    if (collision.gameObject.CompareTag("trap"))
    {
        GameController.instance.charHealth = 0;
        GameController.instance.isFinished = true;
    }

    if (collision.gameObject.CompareTag("power"))
    {
        if (GameController.instance.charHealth <= 75)
        {
            HealthBar.instance.heal(cure);
        }
        else
        {
                //delta de health requerido para que llegue a 100, en caso no haya sido lastimado tanto..
                float requerido = 100 - GameController.instance.charHealth;
                HealthBar.instance.heal(requerido);
        }
    }
    if (collision.gameObject.CompareTag("change"))
    {
        GameController.instance.success = true;

    }

    if (collision.gameObject.CompareTag("map"))
    {
        GameController.instance.successTwo = true;

    }
}

    public void rightDir()
    {
        right = true;
        left = false;
    }
    public void leftDir()
    {
        left = true;
        right = false;
    }
    public void upDir()
    {
        up = true;
    }
    public void stop()
    {
        up = false;
        left = false;
        right = false;
        anim.SetFloat("Speed", 0f);
    }
}
