﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    //variables de objetos
    public GameObject relativeTo;
    SpriteRenderer objRenderer;
    SpriteRenderer thisRenderer;
    AudioSource src;

    //modelajes de arma
    public GameObject bulletRight, bulletLeft;
    Vector2 bulletPos;
    public float fireRate = 0.5f;
    float timeToFire = 0f;

    // para boton
    bool fire=false;
    // Use this for initialization
    void Awake()
    {
        objRenderer = relativeTo.GetComponent<SpriteRenderer>();
        thisRenderer = GetComponent<SpriteRenderer>();
        src = GetComponent<AudioSource>();
        
    }

    // Update is called once per frame
    void Update(){
        flipGun();

        if (fire)
        {
            if (fireRate == 0)
            {

                src.Play();
                Shoot();
            }
            else
            {
                if (Time.time > timeToFire)
                {
                    timeToFire = Time.time + 1 / fireRate;
                    src.Play();
                    Shoot();
                }
            }

        }
       
       


    }
    void Shoot()
    {
        bulletPos = transform.position;
        if (thisRenderer.flipX == true)
        {
            bulletPos += new Vector2(-0.2f, 0.12f);
            

            Instantiate(bulletLeft, bulletPos, Quaternion.identity);

        }
        else
        {
            bulletPos += new Vector2(0.2f, 0.12f);
            Instantiate(bulletRight, bulletPos, Quaternion.identity);

        }
    }
    void flipGun()
    {
        if (objRenderer.flipX == true)
        {

            thisRenderer.flipX = true;

        }
        else
        {

            thisRenderer.flipX = false;
        }
    }
    public void FireButton()
    {
        fire = true;   
    }
    public void stopFire()
    {
        fire = false;
    }

}
